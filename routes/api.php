<?php

use App\Models\Locations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('register', 'Api\RegisterController@register');
Route::post('login', 'Api\RegisterController@login');



Route::middleware('auth:api')->namespace('Api')->group( function () {
    Route::resource('user', 'UserController');
    /**
     * TODO как можно сдесь сделать группу роутов в группе роутов что бы все locations были в одной группе,все hubs в другой и тд)
     *
     */

    Route::resource('locations', 'LocationsController');
    Route::post('/locations/status/{id}','LocationsController@setStatus');
    Route::post('/locations/manager/{id}','LocationsController@setManager');

    Route::resource('hubs', 'HubsController');
    Route::post('/hubs/status/{id}','HubsController@setStatus');

    Route::resource('devices', 'DevicesController');
    Route::post('/devices/status/{id}','DevicesController@setStatus');
});
