<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $fillable = [
        'id', 'name','location_id'
    ];
    public function locations(){
        return $this->belongsTo(Locations::class,'location_id','id');
    }
}
