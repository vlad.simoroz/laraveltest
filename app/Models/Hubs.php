<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hubs extends Model
{
    protected $fillable = [
        'id', 'name', 'status','description','code_hub','id_location'
    ];

    protected $with = ['id_location'];

    public function id_location(){
        return $this->hasOne(Locations::class,'id','id_location');
    }
}
