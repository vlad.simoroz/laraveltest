<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $fillable = [
        'id', 'name', 'status','longitude','latitude','url_google_maps','description','manager_id'
    ];

    protected $with = ['tags','manager_id'];

    public function tags(){
        return $this->hasMany(Tags::class,'location_id');
    }
    public function manager_id(){
        return $this->belongsTo(User::class,'manager_id','id');
    }
}
