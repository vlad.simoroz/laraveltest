<?php

namespace App\Http\Controllers;

use App\Locations;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function index(){
        $locations = Locations::all();
        return response()->json($locations);
    }
}
