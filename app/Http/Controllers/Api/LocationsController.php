<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Base;
use App\Models\Locations;
use App\Models\Tags;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LocationsController extends BaseController
{
    public function index(){
        $locations = Locations::all();
        return $this->sendResponse(new Base($locations), 'ok');
    }
    public function store(Request $request){
        $input = $request->all();

        $validator = Validator::make($input,[
            "name" => "required",
            "longitude" => "required|min:6",
            "latitude" => "required|min:6",
            "url_google_maps" => "url",
            "description" => "min:1|max:255",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input["status"] = 1;
        $input["manager_id"] = Auth::id();


        if(Locations::where([['longitude', '=' ,$input["longitude"]],['latitude' , '=', $input["latitude"]]])->count()>0){
            return $this->sendError('Validation Error.','Location with such longitude/latitude already exists');
        }

        $location = Locations::create($input);

        if (isset($input["tags"])){
            $tags = explode(',',$input["tags"]);
            foreach ($tags as $tag){
                $tag_new = new Tags();
                $tag_new->name = $tag;
                $tag_new->location_id = $location->id;
                $tag_new->save();
            }
        }

        return $this->sendResponse(new Base($location),'Location create ');
    }

    public function show($id){
        $location = Locations::find($id);
        if (is_null($location)){
            return $this->sendError('Find error','error ');
        }
        return $this->sendResponse(new Base($location),'ok ');
    }

    public function update(Request $request , Locations $location){
        $input = $request->only(['name','longitude','latitude','url_google_maps','description']);

        $validator = Validator::make($input,[
            "name" => "min:1",
            "longitude" => "min:6",
            "latitude" => "min:6",
            "url_google_maps" => "url",
            "description" => "min:1|max:255",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $location->fill($input);
        $location->save();

        return $this->sendResponse(new Base($location),'ok ');
    }

    public function setStatus(Request $request , $id){
        $input = $request->only(['status']);
        $location = Locations::find($id);
        $location ->status = $input["status"];
        $location->save();

        return $this->sendResponse(new Base($location),'ok ');
    }

    public function setManager(Request $request , $id){
        $input = $request->only(['manager_id']);
        $manager = User::where('id',$input["manager_id"])->first();
        if (!$manager){
            return $this->sendError("No User",'ok ');
        }
        $location = Locations::find($id);
        $location ->manager_id = $manager->id;
        $location->save();

        return $this->sendResponse(new Base($location),'ok ');
    }
}
