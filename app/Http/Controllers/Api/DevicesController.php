<?php

namespace App\Http\Controllers\Api;

use App\Models\Devices;
use App\Http\Resources\Base;
use App\Models\Hubs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DevicesController extends BaseController
{
    public function index(){
        $hub = Devices::all();
        return $this->sendResponse(new Base($hub), 'ok');
    }

    public function store(Request $request){
        $input = $request->all();

        $validator = Validator::make($input,[
            "name" => "required",
            "code_device" => "required",
            "description" => "min:1|max:255",
            "id_hubs" => "required",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input["status"] = 1;

        $hubs = Hubs::where("id","=",$input["id_hubs"])->count();

        if(is_null($hubs) ){
            return $this->sendError('Validation Error.','Hub is not defend');
        }

        if(Devices::where("code_device","=",$input["code_device"])->count()>0){
            return $this->sendError('Validation Error.','Device with such `code_device` already exists');
        }

        $hub = Devices::create($input);

        return $this->sendResponse(new Base($hub),'Device create ');
    }

    public function show($id){
        $devices = Devices::find($id);
        if (is_null($devices)){
            return $this->sendError('Find error','error ');
        }
        return $this->sendResponse(new Base($devices),'ok ');
    }

    public function update(Request $request , $id){
        $input = $request->only(['name','code_device','id_hubs','description']);

        $validator = Validator::make($input,[
            "name" => "min:1",
            "code_device" => "min:1",
            "description" => "max:255",
            "id_hubs" => "min:1",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if (isset($input["id_hubs"])){
            $hubs = Hubs::where("id","=",$input["id_hubs"])->count();

            if(is_null($hubs) ){
                return $this->sendError('Validation Error.','Hubs is not defend');
            }
        }
        $devices = Devices::find($id);
        $devices->fill($input);
        $devices->save();

        return $this->sendResponse(new Base($devices),'ok ');
    }
    public function setStatus(Request $request , $id){
        $input = $request->only(['status']);
        $devices= Devices::find($id);
        $devices ->status = $input["status"];
        $devices->save();

        return $this->sendResponse(new Base($devices),'ok ');
    }
}
