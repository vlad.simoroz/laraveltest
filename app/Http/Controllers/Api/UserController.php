<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{
    public function index()
    {
        $users = User::all();

        return $this->sendResponse(UserResource::collection($users), 'Users retrieved successfully.');
    }

    public function store(Request $request){
        $input = $request->all();

        $validator = Validator::make($input,[
            "name" => "required",
            "email" => "required|email:rfc,dns",
            "password" => "required|min:6",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input["password"] = Hash::make($input["password"]);

        if(User::where('email',$input["email"])->count()>0){
            return $this->sendError('Validation Error.','User with such mail already exists');
        }

        $user = User::create($input);

        return $this->sendResponse(new UserResource($user),'User create ');
    }

    public function show($id){
        $user = User::find($id);
        if (is_null($user)){
            return $this->sendError('Find error','User not find ');
        }
        return $this->sendResponse(new UserResource($user),'User find ');
    }

    public function update(Request $request , User $user){
        $input = $request->only(["name","email"]);

        $validator = Validator::make($input,[
            "name" => "required",
            "email" => "required|email:rfc,dns"
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if (User::where('email',$input["email"])->count()>0){
            $user->name = $input["name"];
            $user->email = $input["email"];
            $user->save();
            return $this->sendResponse(new UserResource($user),'User update ');
        }

        return $this->sendError('Error create User.', "User not update");
    }
}
