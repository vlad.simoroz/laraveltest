<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Base;
use App\Models\Hubs;
use App\Models\Locations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HubsController extends BaseController
{
    public function index(){
        $hub = Hubs::all();
        return $this->sendResponse(new Base($hub), 'ok');
    }

    public function store(Request $request){
        $input = $request->all();

        $validator = Validator::make($input,[
            "name" => "required",
            "code_hub" => "required",
            "description" => "min:1|max:255",
            "id_location" => "required",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input["status"] = 1;
        $location = Locations::where("id","=",$input["id_location"])->count();

        if(is_null($location) ){
            return $this->sendError('Validation Error.','Location is not defend');
        }

        if(Hubs::where("code_hub","=",$input["code_hub"])->count()>0){
            return $this->sendError('Validation Error.','Hub with such code_hub already exists');
        }

        $hub = Hubs::create($input);

        return $this->sendResponse(new Base($hub),'Hub create ');
    }

    public function show($id){
        $hub = Hubs::find($id);
        if (is_null($hub)){
            return $this->sendError('Find error','error ');
        }
        return $this->sendResponse(new Base($hub),'ok ');
    }

    public function update(Request $request , Hubs $hub){
        $input = $request->only(['name','code_hub','id_location','description']);

        $validator = Validator::make($input,[
            "name" => "min:1",
            "code_hub" => "min:1",
            "description" => "max:255",
            "id_location" => "min:1",
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if (isset($input["id_location"])){
            $location = Locations::where("id","=",$input["id_location"])->count();

            if(is_null($location) ){
                return $this->sendError('Validation Error.','Location is not defend');
            }
        }

        $hub->fill($input);
        $hub->save();

        return $this->sendResponse(new Base($hub),'ok ');
    }

    public function setStatus(Request $request , $id){
        $input = $request->only(['status']);
        $hub= Hubs::find($id);
        $hub ->status = $input["status"];
        $hub->save();

        return $this->sendResponse(new Base($hub),'ok ');
    }
}
